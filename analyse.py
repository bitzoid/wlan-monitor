#!/usr/bin/python3

import datetime
import json

def parselog(fp,only=None,start=None,end=None):
  tests = dict()
  lines = 0
  for line in log:
    lines += 1
    try:
      clean = line.partition('#')[0].strip().replace('\\','\\\\')
      if clean == '':
        continue
      # speedtest can mess up:
      clean = clean.replace(',"speedtest" :',', "speedtest" :').replace(', "speedtest" : ,',',').replace(', "speedtest" : }','}')
      reading = json.loads(clean)
      if only != None and 'speedtest' in reading and reading['speedtest']['server']['cc'] != only:
        continue
      if 'date' not in reading:
        continue
      dt = datetime.datetime.fromisoformat(reading['date'])
      if start != None and dt < start:
        continue
      if end != None and dt > end:
        continue
      if 'ESSID' not in reading:
        for essid in tests.values():
          essid[dt.timestamp] = {'datetime': dt}
          testcase = essid[dt.timestamp]
          if 'iwlist' in reading:
            testcase['cells'] = dict()
            for cell in reading['iwlist'].values():
              channel = dict()
              for key in { "Frequency", "Quality", "Signal level", "ESSID" }:
                if key in cell:
                  channel[key] = cell[key]
              testcase['cells'][int(cell['Channel'])] = channel
        continue
      essid = tests.setdefault(reading['ESSID'],dict())
      testcase = essid.setdefault(dt.timestamp(),dict())
      testcase['datetime'] = dt
      testcase['date'] = reading['date']
      for key,datapoint in [('rate','Bit Rate'),('link','Link Quality'),('level','Signal level'),('noise','Noise')]:
        if datapoint in reading:
          testcase[key] = reading[datapoint]
      if 'level' in testcase and testcase['level'].endswith(' dBm'):
        testcase['signal'] = float(testcase['level'][0:-4])
      if 'hosts' in reading:
        testcase['hosts'] = reading['hosts']
      if 'speedtest' in reading:
        testcase['ping'] = float(reading['speedtest']['ping'])
        testcase['up'] = float(reading['speedtest']['upload'])/1000000
        testcase['down'] = float(reading['speedtest']['download'])/1000000
      else:
        testcase['ping'] = None
        testcase['up'] = None
        testcase['down'] = None
      if 'iwlist' in reading:
        testcase['cells'] = dict()
        testcase['noisy'] = set()
        for cell in reading['iwlist'].values():
          channel = dict()
          if 'ESSID' in cell and cell['ESSID'] == reading['ESSID']:
            continue
          for key in { "Frequency", "Quality", "Signal level", "ESSID" }:
            if key in cell:
              channel[key] = cell[key]
          if 'Signal level' in channel and channel['Signal level'].endswith(' dBm'):
            channel['noise'] = float(channel['Signal level'][0:-4])
            if 'signal' in testcase and channel['noise'] >= testcase['signal']:
              testcase['noisy'].add(int(cell['Channel']))
          testcase['cells'][int(cell['Channel'])] = channel

      else:
        pass
        #import random
        #testcase['cells'] = [0]*int(random.uniform(1,4))
        #testcase['noisy'] = testcase['cells'] + [1]*int(random.uniform(1,3))

    except:
      print('Error: Invalid line {}: "{}"'.format(lines,reading))
      exit(1)
  return tests

def transpose(d, key):
  return list(map(lambda value: value[key],d.values()))

def xy(d, xk, yk):
  def extract(value):
    if xk in value and yk in value:
      return (value[xk],value[yk])
    elif xk in value and yk not in value:
      return (value[xk],None)
    else:
      return None
  return list(zip(*list(filter(None,map(extract, d.values())))))

def filter_days(tests, dow=None):
  out = list()
  known = set()
  for tc in tests.values():
    dt = tc['datetime']
    if dow != None and dt.weekday() != dow:
      continue
    k = (dt.year,dt.month,dt.day)
    if k not in known:
      known.add(k)
      out.append(dt)
  return out

def getnoisecells(tests):
  ret = set()
  for cells in filter(None,xy(tests,'datetime','cells')[1]):
    for cell in cells.values():
      if 'ESSID' in cell:
        ret.add(cell['ESSID'])
  return list(ret)

def plot_setup_(tests,ax):
  ax = ax
  ax.set_xlim(left=transpose(tests,'datetime')[0],right=transpose(tests,'datetime')[-1])

  ax.grid(which='major',axis='x')
  for t in filter_days(tests,0):
    ax.axvline(t,color='gray')

  return ax.twinx()

def none_or_fn(fn):
  def helper(maybe):
    if maybe is None:
      return None
    return fn(maybe)
  return helper

def plot_inet(essid,tests,ax_ms):
  ax_bps = plot_setup_(tests,ax_ms)
  ax_ms.set_title('INET: ' + essid)

  #ax_ms.plot(transpose(tests,'datetime'),transpose(tests,'ping'),color='black',label='ping')
  ax_ms.plot(*xy(tests,'datetime','ping'),  color='black', label='ping')
  ax_bps.plot(*xy(tests,'datetime','down'), color='green', label='down')
  ax_bps.plot(*xy(tests,'datetime','up'),   color='red',   label='up')

  ax_ms.set_ylabel('ping / ms')
  ax_bps.set_ylabel('Data / MBit/s')
  #ax_ms.set_ylim(top=100)
  ax_ms.set_ylim(bottom=0)
  ax_bps.set_ylim(bottom=0)
  ax_ms.legend(loc='upper left')
  ax_bps.legend(loc='upper right')

  ax_ms.set_xticks(filter_days(tests))

def plot_wlan(essid,tests,ax):
  pretty_essid = '{' + essid + '}'
  ax_dbm = plot_setup_(tests,ax)
  ax.set_title('WLAN: ' + essid)
  #ax_lnk = ax
  ax_nse = ax

  extract_link = eval

  @none_or_fn
  def extract_rate(s):
    get = lambda x: float(x[0:-5])
    if s.endswith(' Mb/s'):
      return get(s)
    if s.endswith(' kb/s'):
      return get(s)/1000.
    else:
      return 0.

  @none_or_fn
  def extract_dbm(s):
    get = lambda x: float(x[0:-4])
    if s.endswith(' dBm'):
      return get(s)
    raise ValueError()
    return 0

  extract_stats = none_or_fn(len)

  extract_hosts = none_or_fn(len)

  #ax_lnk.plot(transpose(tests,'datetime'),list(map(extract_link,transpose(tests,'link'))),color='purple',label='Link-Quality')
  #ax_bps.plot(transpose(tests,'datetime'),list(map(extract_rate,transpose(tests,'rate'))),color='blue',label='Rate')
  statsxy = xy(tests,'datetime','cells')
  if len(statsxy) == 2:
    ax_nse.plot(statsxy[0],list(map(extract_stats,statsxy[1])),color='yellow',label='# Total Stations')

  noisyxy = xy(tests,'datetime','noisy')
  if len(noisyxy) == 2:
    ax_nse.plot(noisyxy[0],list(map(extract_stats,noisyxy[1])),color='orange',label='# Noisy Stations (louder than {})'.format(pretty_essid))

  hostsxy = xy(tests,'datetime','hosts')
  if len(hostsxy) == 2:
    ax_nse.plot(hostsxy[0],list(map(extract_hosts,hostsxy[1])),color='purple',label='# Hosts in {}'.format(pretty_essid))

  dbmxy = xy(tests,'datetime','level')
  ax_dbm.plot(dbmxy[0],list(map(extract_dbm,dbmxy[1])),color='black',label='Signal Level')

  for nc in getnoisecells(tests):
    @none_or_fn
    def extract_cell(c):
      for att in c.values():
        for k,v in att.items():
          if k == 'ESSID' and v == nc and 'noise' in att:
            return att['noise']
      return None
    cellxy = xy(tests,'datetime','cells')
    #clean = nc.replace('\\\\','\\')
    clean = eval('"' + nc + '"').replace('\x00','?')
    # legacy extractor
    #cellxy = list(zip(*list(filter((lambda tup: tup[1] != None),list(zip(cellxy[0],list(map(extract_cell,list(cellxy[1])))))))))
    cellxy[1] = list(map(extract_cell,list(cellxy[1])))
    if len(cellxy) == 2:
      ax_dbm.plot(list(cellxy[0]),list(cellxy[1]),color='gray')
    else:
      print('Warning: not drawing station {}'.format(nc))
    #ax_dbm.plot(cellxy[0],list(map(extract_cell,list(cellxy[1]))),color='gray',label=('{'+clean+'}'))
    #print(list(map(extract_cell,)))


  ax_nse.set_ylabel('# / 1')
  #ax_lnk.set_ylabel('Link-Quality / 1')
  #ax_bps.set_ylabel('Rate / MBit/s')
  ax_dbm.set_ylabel('Signal Level / dBm')
  #ax_lnk.set_ylim(bottom=0)
  #ax_lnk.set_ylim(top=1)
  ax_nse.set_ylim(bottom=0)
  #ax_bps.set_ylim(bottom=0)
  #ax_lnk.legend(loc='center left')
  ax_nse.legend(loc='lower left')
  #ax_bps.legend(loc='upper right')
  ax_dbm.legend(loc='upper left')

  ax.set_xticks(filter_days(tests))

def plot(essid,tests,out=False):
  import matplotlib.pyplot as plt
  import matplotlib.ticker as ticker
  import matplotlib.dates as md
  if out:
    from matplotlib import use
    use('AGG')

  fig, ax = plt.subplots(2,figsize=(16,9))
  print('  Plotting WLAN')
  plot_wlan(essid,tests,ax[0])
  print('  Plotting INET')
  plot_inet(essid,tests,ax[1])

  #plt.gca().xaxis.set_major_formatter(md.DateFormatter('%Y-%m-%d\n%_H Uhr'))
  plt.gca().xaxis.set_major_formatter(md.DateFormatter('%Y-%m-%d'))
  fig.autofmt_xdate(rotation=45)

  if out:
    print('  Emitting {}.png'.format(essid))
    plt.savefig(essid+'.png',dpi=300)
  else:
    print('  Opening interactive plot viewer')
    plt.show()

import argparse

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--start", help="Start plot at day. Eg: --start=2020-10-01", type=datetime.datetime.fromisoformat)
  parser.add_argument("--end", help="End plot at day. Eg: --end=2020-11-01", type=datetime.datetime.fromisoformat)
  parser.add_argument("--log", help="Logfile", default="iw.log")
  parser.add_argument("--interactive", help="Do not export png files, but open interactive display.", action='store_true', default=False)
  args = parser.parse_args()
  with open(args.log) as log:
    for essid, tests in parselog(log,only='DE',start=args.start, end=args.end).items():
      print('Plotting essid: {}'.format('{' + essid + '}'))
      plot(essid, tests, not args.interactive)
