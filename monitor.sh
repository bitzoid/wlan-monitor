#!/bin/bash

MONITOR_LOG_DEFAULT='/home/pi/iw.log'
true ${MONITOR_LOG:=$MONITOR_LOG_DEFAULT}

MONITOR_DEV_DEFAULT='wlan0'
true ${MONITOR_DEV:=$MONITOR_DEV_DEFAULT}

function logwifi() {
  (
    interface="$MONITOR_DEV"

    date '+"date" : "%F %T"'

    reading="$(iwconfig $interface)"

    echo "$reading" | sed -n 's/^.*\(ESSID\): *\(".*"\) *$/"\1": \2/p'
    for data in 'Bit Rate' 'Link Quality' 'Signal level'; do
      echo "$reading" | sed -n 's/^.*\(\('"$data"'\)=\([^ ]\+\( [^ ]\+\)\?\)\).*$/"\2" : "\3" /p'
    done

    iwlist="$(sudo -n iwlist "$interface" scan 2>/dev/null || iwlist "$interface" scan 2>/dev/null)"
    #iwlist="$(cat iwlist.scan)"

    echo "$iwlist" |
      sed 's/^ \+/\t/'           | # pre-process nested record members
      tr '\n' '\r'               | # this is easier to use in sed (but slows down sed a bit)
      sed 's/\r\tCell/\nCell/g'  | # record boundary found by hacking the string apart at "Cell" tokens
      tail -n +2                 | # the first line is not a record
      sed 's/^\(Cell \+[0-9]\+\) \+- \+\(.\+\)$/"\1" : {\r\t\2\r}/g'      | # jsonify record on top level, i.e. "Cell" : { UNFORMATTED STUFF }
      sed 's/Quality=/Quality:/g; s/ *Signal level=/\r\tSignal level:/g'  | # tidy up some idiosyncracies
      sed 's/\r\t\([^:]\+\r\)/\1/g'                                       | # join lines for entries that span more than one line
      sed 's/\t\([^:]\+\): *\([^\r]*[^\r ]\) *\r/\t"\1": "\2"\r/g'        | # jsonify individual record members, i.e. "key": "value"
      sed 's/""/"/g'             | # fix double quotation (some record members already had quotes around their values)
      sed 's/\r\t/, /g'          | # join all record members and glue them together with commas instead or \r tokens
      sed 's/{,/{/g; s/\r}/ }/g' | # fix erroneous delimeters at beginning and end
      tr '\r\n' ' ,'             | # join all record into one line (glue with comma instead of \n)
      sed 's/^.*$/"iwlist" : { & }/g' | # finally wrap json record-glob into set with key "iwlist"
      sed 's/$/\n/'                     # end line

    ip="$(ip -4 -o a | grep "$interface" | sed 's/^.*inet \([^ ]\+\) .*$/\1/')"
    nmap -sn "$ip" |
      sed -n 's/^Nmap scan report for \(.*\)$/"\1"/p' |
      tr '\n' ',' |
      sed 's/^\(.*[^,]\),\?$/"hosts": [ \1 ]\n/'

    echo -n '"speedtest" : '
    speedtest --json

  ) | sed 's/ *$//' | tr '\n' ',' | sed 's/, *$//' | sed 's/^.*$/{ & }\n/' \
  >> "$MONITOR_LOG"
}

if [ "$1" == '--daemon' ]; then

  # wait for next full hour
  while sleep "$[ $(date -d "$(date '+%F %H')" '+%s') + 3600 - $(date '+%s') ]"; do

    logwifi
    sleep 5m

  done

else

  logwifi

fi
